package com.example.firbaseregistration

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {
    lateinit var emailfield:EditText
    lateinit  var password1:EditText
    lateinit var registerbtn : Button
    lateinit var retypepassword :EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        registration()




    }


    fun registration(){
        emailfield=findViewById(R.id.emailaddressfield)
        password1=findViewById(R.id.passwordfield)
         registerbtn =findViewById(R.id.registerbutton)
         retypepassword=findViewById(R.id.retypepasswordinput)


        registerbtn.setOnClickListener {
            var inputedemail=emailfield.text
            var inputedpassword=password1.text
            var inputedpassword2=retypepassword.text


            if(  !inputedemail.isEmpty() && inputedemail.contains("@") && !inputedpassword.isEmpty()  &&  inputedpassword.length>=8 && inputedpassword.toString().equals(inputedpassword2.toString())){

                FirebaseAuth.
                getInstance().
                createUserWithEmailAndPassword(inputedemail.toString(),inputedpassword.toString()).
                addOnCompleteListener {  task ->
                    if(task.isSuccessful) {
                        Toast.makeText(this,"Your are Succesfuly registered !",Toast.LENGTH_SHORT).show()  }
                    else{ Toast.makeText(this,"Error happend , Email already exists or something went wrong on the server  !",Toast.LENGTH_SHORT).show()     } }


            }
            if(inputedemail.isEmpty() || inputedpassword.isEmpty() || inputedpassword2.isEmpty()){

            Toast.makeText(this,"Please Input every field!",Toast.LENGTH_SHORT).show()

        }
           else  if( inputedpassword.length<8  && inputedpassword2.length<8){

            Toast.makeText(this,"Passwrod must be more than 8 characters long ",Toast.LENGTH_SHORT).show()

        }
              if( !inputedpassword.toString().equals(inputedpassword2.toString())){

                Toast.makeText(this,"Passwords does not match  ",Toast.LENGTH_SHORT).show()

            }
            else  if( !inputedemail.contains("@")&& !inputedemail.contains(".")){

                Toast.makeText(this," Email input is invalid",Toast.LENGTH_SHORT).show()

            }










        }












    }



}